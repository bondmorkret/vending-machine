﻿namespace Infrastructure.Models
{
    public enum StockType
    {
        Cola,
        Chips,
        Candy
    }
}