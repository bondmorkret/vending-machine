﻿using System.Collections.Generic;
using System.Linq;

using Infrastructure.Models;
using Infrastructure.Data;

namespace Infrastructure.Repositories
{
    public class StockRepository : IStockRepository
    {
        private readonly Stock _stock = new Stock();

        // Gets the current stock levels from our data source, which would be a database
        public List<StockItem> GetStock()
        {
            return _stock.Items;
        }

        public StockItem GetStockItem(int id)
        {
            return _stock.Items.FirstOrDefault(item => item.Id == id);
        }

        // Removes an item from stock when it is purchased
        public bool RemoveItem(StockType stockType)
        {
            StockItem stockItem = _stock.Items.FirstOrDefault(item => item.Type == stockType);

            if (stockItem != null)
            {
                // We would need to persist this change in the database
                stockItem.Quantity -= 1;
                return true;
            }

            return false;
        }
    }
}