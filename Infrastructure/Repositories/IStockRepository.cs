﻿using System.Collections.Generic;

using Infrastructure.Models;

namespace Infrastructure.Repositories
{
    public interface IStockRepository
    {
        List<StockItem> GetStock();

        StockItem GetStockItem(int id);

        bool RemoveItem(StockType stockType);
    }
}