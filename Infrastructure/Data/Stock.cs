﻿using System.Collections.Generic;

using Infrastructure.Models;

namespace Infrastructure.Data
{
    public class Stock
    {
        // This is a hardcoded representation of our stock, which would be stored in a database

        public List<StockItem> Items = new List<StockItem>
        {
            new StockItem
            {
                Id = 1,
                Name = "Cola",
                Image = "/Content/Cola.png",
                Type = StockType.Cola,
                Description = "A fizzy refreshing drink!",
                Price = 100,
                Quantity = 10
            },
            new StockItem
            {
                Id = 2,
                Name = "Chips",
                Image = "/Content/Chips.png",
                Type = StockType.Chips,
                Description = "Salt and vinegar flavour",
                Price = 50,
                Quantity = 20
            },
            new StockItem
            {
                Id = 3,
                Name = "Candy",
                Image = "/Content/Candy.png",
                Type = StockType.Candy,
                Description = "For the sweet tooth",
                Price = 65,
                Quantity = 3
            }
        };
    }
}