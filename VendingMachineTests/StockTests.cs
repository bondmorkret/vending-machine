﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Infrastructure.Repositories;
using Infrastructure.Models;

namespace VendingMachineTests
{
    [TestClass]
    public class StockTests
    {
        private readonly IStockRepository _stockRepository = new StockRepository();

        [TestMethod]
        public void ShouldReturnCorrectStockLevels()
        {
            List<StockItem> stock = _stockRepository.GetStock();

            // There should be 10 cola items when stock is full
            Assert.AreEqual(10, stock.First(item => item.Type == StockType.Cola).Quantity);

            // There should be 20 chips items when stock is full
            Assert.AreEqual(20, stock.First(item => item.Type == StockType.Chips).Quantity);

            // There should be 10 cola items when stock is full
            Assert.AreEqual(3, stock.First(item => item.Type == StockType.Candy).Quantity);
        }

        [TestMethod]
        public void ShouldReturnCorrectStockItem()
        {
            StockItem item = _stockRepository.GetStockItem(1);

            // This stock item should be cola
            Assert.AreEqual(StockType.Cola, item.Type);
        }

        [TestMethod]
        public void ShouldUpdateStockLevels()
        {
            List<StockItem> stock = _stockRepository.GetStock();

            // There should be 10 cola items when stock is full
            Assert.AreEqual(10, stock.First(item => item.Type == StockType.Cola).Quantity);

            // Remove a cola item as it has been purchases
            _stockRepository.RemoveItem(StockType.Cola);

            // Get an updated stock list
            stock = _stockRepository.GetStock();

            // There should be 9 cola items when we've removed one
            Assert.AreEqual(9, stock.First(item => item.Type == StockType.Cola).Quantity);
        }
    }
}
