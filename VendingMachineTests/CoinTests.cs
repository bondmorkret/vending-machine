﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Domain.Coins;

namespace VendingMachineTests
{
    [TestClass]
    public class CoinTests
    {
        private readonly ICoinService _coinService = new CoinService();

        [TestMethod]
        public void ShouldReturnCorrectTotal()
        {
            // Create some coin tokens
            List<CoinToken> coins = new List<CoinToken>
            {
                new CoinToken
                {
                    Weight = 1
                },
                new CoinToken
                {
                    Weight = 4
                },
                new CoinToken
                {
                    Weight = 5
                },
                new CoinToken
                {
                    Weight = 12
                },
            };

            // Get the total from the coin service
            double total = _coinService.GetTotal(coins);

            Assert.AreEqual(113, total);
        }
    }
}
