﻿using System.Collections.Generic;

using Infrastructure.Models;

namespace VendingMachine.ViewModels
{
    public class StockApiViewModel
    {
        public List<StockItem> Items { get; set; }
    }
}