﻿namespace VendingMachine.ViewModels
{
    public class PurchaseApiViewModel
    {
        public bool Success { get; set; }

        public double Change { get; set; }
    }
}