﻿using Infrastructure.Models;
using Infrastructure.Repositories;
using VendingMachine.ViewModels;

namespace VendingMachine.Factories
{
    public class PurchaseApiViewModelFactory
    {
        private StockRepository _stockRepository = new StockRepository();

        // Get the viewmodel that the API will serialize to JSON
        public PurchaseApiViewModel Create(double total, StockItem item)
        {
            /* If enough money is supplied, the item Id was valid, and the stock repository is able to allocate one 
                 * item of stock, we can approve the transaction */

            bool transactionPossible = total >= item.Price &&
                                       item != null &&
                                       _stockRepository.RemoveItem(item.Type);

            return new PurchaseApiViewModel
            {
                Success = transactionPossible,
                Change = transactionPossible ? total - item.Price : total
            };
        }
    }
}