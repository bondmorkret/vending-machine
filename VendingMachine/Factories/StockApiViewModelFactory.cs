﻿using Infrastructure.Repositories;
using VendingMachine.ViewModels;

namespace VendingMachine.Factories
{
    public class StockApiViewModelFactory
    {
        private StockRepository _stockRepository = new StockRepository();

        // Get the viewmodel that the API will serialize to JSON
        public StockApiViewModel Create()
        {
            return new StockApiViewModel
            {
                Items = _stockRepository.GetStock()
            };
        }
    }
}