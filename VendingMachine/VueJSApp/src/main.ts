import * as Vue from 'vue';

import { HomeComponent } from './components/home/home-component';

new Vue({
  el: '#app-main',
  components: {
    'home': HomeComponent
  }
});
