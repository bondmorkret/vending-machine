export class Stock {

    constructor(
        items: StockItem[]
    ) {
        this.Items = items;
    }

    Items: StockItem[];
}

export class StockItem {

    constructor(
        id: number,
        name: string,
        image: string,
        type: number,
        description: string,
        price: number,
        quantity: number
    ) {
        this.Id = id;
        this.Name = name;
        this.Image = image;
        this.Type = type;
        this.Description = description;
        this.Price = price;
        this.Quantity = quantity;
    }

    Id: number;
    Name: string;
    Image: string;
    Type: number;
    Description: string;
    Price: number; 
    Quantity: number;
}