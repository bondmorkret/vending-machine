import Vue from 'vue';
import Component from 'vue-class-component';

import axios from 'axios'
import VueAxios from 'vue-axios'

import { StockItem } from './stock';
import { Coin } from './coin';
import { CoinToken } from './coin-token';
import { PurchaseResponse } from './purchase-response';
 
Vue.use(VueAxios, axios)

@Component({
    template: require('./home.html')
})
export class HomeComponent extends Vue {

    public stockItems: StockItem[] = [];
    public purchasedItems: StockItem[] = [];
    public inserted: Coin[] = [];
    public coinTypes: Coin[] = [new Coin('$2', 200, 15), new Coin('$1', 100, 12), new Coin('50¢', 50, 10), new Coin('20¢', 20, 3), new Coin('10¢', 10, 4), new Coin('5¢', 5, 2), new Coin('2¢', 2, 5), new Coin('1¢', 1, 1)]
    public insertedTotal: number = 0;
    public rejected: Coin[] = [];
    public showThankyou: boolean = false;
    public showSorry: boolean = false;
    public sorryMessage: string = "";

    created() {
        this.getStock();
    }

    // Called when a coin is inserted
    public insertCoin(coin: Coin): void {
        this.inserted.push(coin);
        this.insertedTotal += coin.Value;
    }

    // Called when the user returns the coins they've inserted
    public returnCoins(): void {
        var coinsCount = 0;

        // Loop around the different types of coin, starting with the largest
        for (var i=0; i < this.coinTypes.length; i++) {
            // If there's still inserted coins left, and they total to more than the current coin type
            if (this.insertedTotal > 0 && this.insertedTotal / this.coinTypes[i].Value >= 1) {
                // Track how many of this type of coin we can include in the rejected coins
                coinsCount = Math.floor(this.insertedTotal / this.coinTypes[i].Value)
                // Adjust the total of coins left, after these coins have been rejected
                this.insertedTotal = this.insertedTotal % this.coinTypes[i].Value;

                // Loop around each coin of this type to reject, and reject it
                for (var j=0; j < coinsCount; j++) {
                    this.rejected.push(new Coin(this.coinTypes[i].Name, this.coinTypes[i].Value, this.coinTypes[i].Weight));
                }
            }
        }
        // Reset our insertion properties as we've rejected all the coins
        this.inserted = [];
        this.insertedTotal = 0;
    }

    public buy(item: StockItem): void {

        let request = {
            Coins: this.inserted,
            ItemId: item.Id
        };

        axios.post("/API/PurchaseApi/BuyItem", request).then(response => {
            let purchaseResponse = <PurchaseResponse>response.data;
            // Update the coins to return based on the API's change response
            this.insertedTotal = purchaseResponse.Change;

            if (purchaseResponse.Success) {
                this.dispenseItem(item);
                // Return any remaining coins
                this.returnCoins();
                this.showMessage("thankyou");
            } else {
                this.sorryMessage = this.getPriceFormatted(item.Price);
                this.showMessage("sorry");
            }
        })
    }

    // Called when the user collects a coin from the change drawer
    public collectCoin(index: number): void {
        this.rejected.splice(index, 1);
    }

    // Called when the user collects a purchased item
    public collectItem(index: number): void {
        this.purchasedItems.splice(index, 1);
    }

    // Makes a nicely formatted string representation of a total of pence
    public getPriceFormatted(total: number): string {
        if (total >= 100) {
            return "$" + (total / 100).toFixed(2);
        } else {
            return total + "¢";
        }
    }

    // Gets the current stock from the API
    private getStock(): void {
        axios.get("/API/StockApi/GetStock")
            .then(response => {
                this.stockItems = <StockItem[]>response.data.Items
            })
    }

    // Called when an item is successfully purchased
    private dispenseItem(item: StockItem) {
        // Push the item into the purchased items
        this.purchasedItems.push(item);
        // Get an updated stock level from the Stock API
        this.getStock();
    }

    private showMessage(message: string) {
        if (message === "thankyou") {
            this.showThankyou = true
            setTimeout( () => this.showThankyou = false , 2000 );
        };
        if (message === "sorry") {
            this.showSorry = true
            setTimeout( () => this.showSorry = false , 2000 );
        };
    }
}
