export class Coin {

    constructor(
        name: string,
        value: number,
        weight: number
    ) {
        this.Name = name;
        this.Value = value;
        this.Weight = weight;
    }

    Name: string;
    Value: number;
    Weight: number;
}