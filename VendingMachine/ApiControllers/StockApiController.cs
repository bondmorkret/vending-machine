﻿using System.Web.Http;

using VendingMachine.Factories;
using VendingMachine.ViewModels;

namespace VendingMachine.ApiControllers
{
    public class StockApiController : ApiController
    {
        private readonly StockApiViewModelFactory _viewModelFactory = new StockApiViewModelFactory();

        // GET: /API/StockApi/GetStock
        public StockApiViewModel GetStock()
        {
            // Get the viewmodel from the factory
            return _viewModelFactory.Create();
        }
    }
}