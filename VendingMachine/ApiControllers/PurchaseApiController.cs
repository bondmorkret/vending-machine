﻿using System.Web.Http;

using VendingMachine.Factories;
using VendingMachine.ViewModels;
using Domain.Coins;
using Infrastructure.Repositories;
using Infrastructure.Models;
using VendingMachine.FormModels;

namespace VendingMachine.ApiControllers
{
    public class PurchaseApiController : ApiController
    {
        private readonly CoinService _coinService = new CoinService();
        private readonly StockRepository _stockRepository = new StockRepository();
        private readonly PurchaseApiViewModelFactory _viewModelFactory = new PurchaseApiViewModelFactory();

        // POST: StockApiController/BuyItem
        [HttpPost]
        public PurchaseApiViewModel BuyItem(PurchaseApiFormModel formModel)
        {
            // Calculate how much change was inserted
            double total = _coinService.GetTotal(formModel.Coins);

            // Allocate an item of stock
            StockItem item = _stockRepository.GetStockItem(formModel.ItemId);

            // Get the viewmodel from the factory
            return _viewModelFactory.Create(total, item);
        }
    }
}