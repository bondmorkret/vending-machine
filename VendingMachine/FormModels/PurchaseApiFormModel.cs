﻿using System.Collections.Generic;

using Domain.Coins;

namespace VendingMachine.FormModels
{
    public class PurchaseApiFormModel
    {
        public List<CoinToken> Coins { get; set; }

        public int ItemId { get; set; }
    }
}