﻿using System.Collections.Generic;

namespace Domain.Coins
{
    public interface ICoinService
    {
        double GetTotal(List<CoinToken> coinTokens);
    }
}