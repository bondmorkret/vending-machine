﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Coins
{
    public class CoinService : ICoinService
    {
        private readonly CoinWeights _coinWeights = new CoinWeights();

        // Gets the total when supplied with some coins
        public double GetTotal(List<CoinToken> coinTokens)
        {
            double total = 0.00;

            // Loop through the coins and match them to a value based on their weight
            foreach (var coinToken in coinTokens)
            {
                /* In a later implementation we may want to explore some tolerance on expected weight,
                 * so we can handle weights which are slightly off expectation, but still return the closest weighted coin. */
                Coin coin = _coinWeights.Coins.FirstOrDefault(c => c.Weight == coinToken.Weight);

                total += coin.Value;
            }

            return total;
        }
    }
}