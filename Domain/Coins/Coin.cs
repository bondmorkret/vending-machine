﻿namespace Domain.Coins
{
    public class Coin
    {
        public int Value { get; set; }

        public int Weight { get; set; }
    }
}