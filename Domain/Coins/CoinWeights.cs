﻿using System.Collections.Generic;

namespace Domain.Coins
{
    public class CoinWeights
    {
        // This is our hardcoded business logic that represents coins and their weights
        public List<Coin> Coins = new List<Coin>
        {
            new Coin
            {
                Value = 1,
                Weight = 1
            },
            new Coin
            {
                Value = 2,
                Weight = 5
            },
            new Coin
            {
                Value = 5,
                Weight = 2
            },
            new Coin
            {
                Value = 10,
                Weight = 4
            },
            new Coin
            {
                Value = 20,
                Weight = 3
            },
            new Coin
            {
                Value = 50,
                Weight = 10
            },
            new Coin
            {
                Value = 100,
                Weight = 12
            },
            new Coin
            {
                Value = 200,
                Weight = 15
            }
        };
    }
}